# Multiplex_EcoSystem

Low Level Design for a Multiplex Ecosystem. Providing services like Add/Remove Multiplex/Screens, Book tickets and Browse through available seats, sorting by price or location. Focus is on learning SOLID principles than accuracy.


## Multiplex Ecosystem should have following services.

```
---------------------------------------
Choose one of the option!
0 - CREATE_MULTIPLEX - ex.  0 PVR 1 1,  PVR -> Multiplex Name, 1 -> X, 1 -> y
1 - REMOVE_MULTIPLEX - ex.  1 PVR 1 1,  PVR -> Multiplex Name, 1 -> X, 1 -> y
2 - SHOW_MULTIPLEX - ex.  2
3 - ADD_SCREEN - ex.  3 PVR 1 1 A 60 30 10,  PVR -> Multiplex Name, 1 -> X, 1 -> y, A -> Screen, 60 -> Total Silevr Seats, 30 -> Gold, 10 -> Platinum
4 - REMOVE_SCREEN - ex.  4 PVR 1 1 A,  PVR -> Multiplex Name, 1 -> X, 1 -> y, A -> Screen
5 - ADD_SHOW - ex.  5 PVR 1 1 A 20220321 1200 1500 200 250 300 thor eng,  PVR -> Multiplex Name, 1 -> X, 1 -> y, A -> Screen, 1200 -> From Time, 1500 -> To TIME, 200 -> Silver Price, 250 -> Gold,300 -> Platinum
6 - REMOVE_SHOW - ex.  6 PVR 1 1 A 20220321 1200 1500,  PVR -> Multiplex Name, 1 -> X, 1 -> y, A -> Screen, 1200 -> From Time, 1500 -> To TIME
7 - BOOK_SHOW - ex.  7 PVR 1 1 A 20220321 1200 9 7 1,  PVR -> Multiplex Name, 1 -> X, 1 -> y, A -> Screen, 1200 -> From Time, 9 -> Silver Seats, 7 -> Gold Seats, 1 -> Platinum Seats
8 - BROWSE - ex.  8 2 3 joker eng,  (1 -> Sort By Price, 2 -> Sort By Proximity), (1 -> Filter By Movie Name, 2 -> Filter By Movie Language, 3 -> Filter By Movie Name and Movie Language), joker -> Movie Name, eng -> Movie Language
9 - EXIT - ex.  9
---------------------------------------
```


* ##### Add Multiplex
    ```
    foo@bar:-$ 0 PVR 1 1
    Multiplex PVR added at (1,1)
    ```
* ##### Remove Multiplex
    ```
    foo@bar:-$ 1 PVR 1 1
    Multiplex PVR removed at (1,1)
    ```
* ##### Show Multiplexes
    ```
    foo@bar:-$ 2
    Here's all Multiplexes!
    Multiplex{multiplexName='Cinepolis', multiplexLocation=(-1,0), Screens = [Screen{screenId=A, Seats=[Silver-60, Gold-30, Platinum-10]}, Screen{screenId=B, Seats=[Silver-60, Gold-30, Platinum-10]}]}
    Multiplex{multiplexName='ABC', multiplexLocation=(1,1), Screens = []}
    Multiplex{multiplexName='PVR', multiplexLocation=(1,2), Screens = [Screen{screenId=A, Seats=[Silver-60, Gold-30, Platinum-10]}, Screen{screenId=B, Seats=[Silver-49, Gold-49, Platinum-2]}]}
    ```
 * ##### Add Screen
     ```
     foo@bar:-$ 3 PVR 1 1 A 60 30 10
     Screen-A added in PVR[1,1] with [Silver-60, Gold-30, Platinum-10] capacity.
     ```
* ##### Remove Screen
     ```
     foo@bar:-$ 3 PVR 1 1 A
     Screen-A removed from PVR[1,1]
     ```
* ##### Add Show
     ```
     foo@bar:-$ 5 PVR 1 1 A 20220321 1200 1430 200 250 300 Joker eng
     Joker[eng] show added at screen-A for 20220321 1200-1430 in PVR at (1,1).   
     show added!
     ```
* ##### Remove Show
   ```
   foo@bar:-$ 6 PVR 1 1 A 20220321 1200 1500
   Show removed at screen-A for 20220321 1200-1500 in PVR at (1,1).   
   show removed!
   ```
* ##### Browse Show
  sorted by price, filter by movie name **se7en**
  ``` 
  foo@bar:-$ 8 1 1 se7en
     Movie       Language      Multiplex  Location    Screen       Category     Seats                Date      Time     Price  Distance
     se7en            eng            PVR     (2,1)         C       PLATINUM         1          2022-03-21     12:00     249.0      2.24
     se7en            eng            PVR     (2,1)         C           GOLD        49          2022-03-21     12:00     269.0      2.24
     se7en          hindi            PVR     (2,1)         C       PLATINUM         1          2022-03-21     18:00     315.0      2.24
     se7en            eng            PVR     (2,1)         C         SILVER        50          2022-03-21     12:00     345.0      2.24
     se7en          hindi            PVR     (2,1)         C           GOLD        49          2022-03-21     18:00     349.0      2.24
     se7en          hindi            PVR     (2,1)         C         SILVER        50          2022-03-21     18:00     395.0      2.24
    ```
  sorted by proximity, filter by movie name and language **forrest_gump** - **hindi**
  ``` 
  foo@bar:-$ 8 2 3 se7en eng
         Movie       Language      Multiplex  Location    Screen       Category     Seats                Date      Time     Price  Distance
  forrest_gump          hindi      Cinepolis    (-1,0)         B         SILVER        60          2022-03-21     18:00     267.0       1.0
  forrest_gump          hindi      Cinepolis    (-1,0)         B           GOLD        30          2022-03-21     18:00     203.0       1.0
  forrest_gump          hindi      Cinepolis    (-1,0)         B       PLATINUM        10          2022-03-21     18:00     184.0       1.0
  forrest_gump          hindi            PVR     (2,1)         A         SILVER        60          2022-03-21     15:00     420.0      2.24
  forrest_gump          hindi            PVR     (2,1)         A           GOLD        25          2022-03-21     15:00     390.0      2.24
  forrest_gump          hindi            PVR     (2,1)         A       PLATINUM        15          2022-03-21     15:00     370.0      2.24
  forrest_gump          hindi            PVR     (2,1)         A         SILVER        60          2022-03-21     18:00     500.0      2.24
  forrest_gump          hindi            PVR     (2,1)         A           GOLD        25          2022-03-21     18:00     475.0      2.24
  forrest_gump          hindi            PVR     (2,1)         A       PLATINUM        15          2022-03-21     18:00     400.0      2.24
  forrest_gump          hindi            PVR     (2,1)         B         SILVER        50          2022-03-21     18:00     395.0      2.24
  forrest_gump          hindi            PVR     (2,1)         B           GOLD        45          2022-03-21     18:00     349.0      2.24
  forrest_gump          hindi            PVR     (2,1)         B       PLATINUM         5          2022-03-21     18:00     315.0      2.24
  forrest_gump          hindi      Cinepolis    (2,-1)         B         SILVER        60          2022-03-21     18:00     450.0      2.24
  forrest_gump          hindi      Cinepolis    (2,-1)         B           GOLD        30          2022-03-21     18:00     415.0      2.24
  forrest_gump          hindi      Cinepolis    (2,-1)         B       PLATINUM        10          2022-03-21     18:00     380.0      2.24
    ```
* ##### Book Seats
  ```
  foo@bar:-$ 7 PVR 2 1 C 20220321 1200 0 0 1
  [Silver-0, Gold-0, Platinum-1] tickets booked for the show 20220321 1200 in PVR at (2,1).
  foo@bar:-$ 7 PVR 2 1 C 20220321 1200 1 0 1
  Not Enough Seats!
  ```