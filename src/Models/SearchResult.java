package Models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

public class SearchResult implements Comparable<SearchResult> {

    public static String OUTUPT_FORMAT = "%30s%15s%15s%10s%10s%15s%10s%20s%10s%10s%10s";

    public static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    public static DateFormat tf = new SimpleDateFormat("HH:mm");

    public String movie;
    public String movieLanguage;
    public String multiplex;
    public int x;
    public int y;
    public String screen;
    public String category;
    public int seats;
    public String date;
    public String time;
    public float price;
    public double distance;

    public SearchResult(String movie, String movieLanguage, String multiplex, int x, int y, char screen, String category, int seats, long time, float price, Location distance) {
        this.movie = movie;
        this.movieLanguage = movieLanguage;
        this.multiplex = multiplex;
        this.x = x;
        this.y = y;
        this.screen = String.valueOf(screen);
        this.category = category;
        this.seats = seats;
        Date date = new Date(time);
        this.date = df.format(date);
        this.time = tf.format(date);
        this.price = price;
        this.distance = Math.round(100*distance.point.distance(Customer.currPoint))/100.0;
    }

    @Override
    public String toString() {
        return String.format(OUTUPT_FORMAT,
                movie,
                movieLanguage,
                multiplex,
                String.format("("+this.x+","+this.y+")"),
                screen,
                category,
                seats,
                date,
                time,
                price,
                distance );
    }

    @Override
    public int compareTo(SearchResult searchResult) {
        if(this.price < searchResult.price)
            return -1;
        else if(this.price > searchResult.price)
            return 1;
        else
            return 0;
    }
}