package Models;

public final class TimeInterval{
    final long fromTime;

    final long toTime;
    public TimeInterval(long fromTime, long toTime){
        this.fromTime=fromTime;
        this.toTime=toTime;
    }

    public long getFromTime() {
        return fromTime;
    }

    public long getToTime() {
        return toTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimeInterval that = (TimeInterval) o;
        return (fromTime >= that.fromTime && fromTime <= that.toTime) || (toTime >= that.fromTime && toTime <= that.toTime);
    }

    @Override
    public int hashCode() {
        return 1; //equals will decide
    }
}
