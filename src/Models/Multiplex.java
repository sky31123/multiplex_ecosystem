package Models;

import java.util.*;

public class Multiplex implements Comparable<Multiplex>{
    String multiplexName;
    Location multiplexLocation;
    Map<Integer, Screen> screens;

    public Map<Integer, Screen> getScreens() {
        return screens;
    }

    public String getMultiplexName() {
        return multiplexName;
    }

    public Location getMultiplexLocation() {
        return multiplexLocation;
    }

    public Multiplex(String multiplexName, Location multiplexLocation){
        this.multiplexName=multiplexName;
        this.multiplexLocation=multiplexLocation;
        this.screens=new HashMap<>();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Multiplex multiplex = (Multiplex) o;
        return Objects.equals(multiplexName, multiplex.multiplexName) && multiplex.multiplexLocation.equals(this.multiplexLocation);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(multiplexName+multiplexLocation.point.x+multiplexLocation.point.y);
    }

    @Override
    public int compareTo(Multiplex multiplex) {
        return this.multiplexLocation.compareTo(multiplex.multiplexLocation);
    }

    @Override
    public String toString() {
        return "Multiplex{" +
                "multiplexName='" + multiplexName + '\'' +
                ", multiplexLocation=" + multiplexLocation.toString() +
                ", Screens = "+screens.values().toString()+
                '}';
    }
}
