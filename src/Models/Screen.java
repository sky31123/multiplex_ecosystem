package Models;

import java.util.*;

public class Screen {
    final char screenId;
    Seats capacity;

    public Map<TimeInterval, Show> getShowTimes() {
        return showTimes;
    }

    Map<TimeInterval, Show> showTimes;
    public Screen(char screenId){
        this.screenId=screenId;
    }

    public Screen(char screenId, Seats capacity){
        this.screenId=screenId;
        this.capacity=capacity;
        showTimes = new HashMap<>();
    }

    public char getScreenId() {
        return screenId;
    }


    public Seats getCapacity() {
        return capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Screen screen = (Screen) o;
        return this.screenId == screen.screenId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(screenId);
    }

    @Override
    public String toString() {
        return "Screen{" +
                "screenId=" + screenId +
                ", Seats=" + capacity.toString() +
                '}';
    }
}


