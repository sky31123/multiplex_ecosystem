package Models;

import java.util.*;

public final class Command {
    public Command_Name command;
    public final List<String> params;

    public static enum Command_Name{
        CREATE_MULTIPLEX,
        REMOVE_MULTIPLEX,
        SHOW_MULTIPLEX,
        ADD_SCREEN,
        REMOVE_SCREEN,
        ADD_SHOW,
        REMOVE_SHOW,
        BOOK_SHOW,
        BROWSE,
        EXIT;
        private static Command_Name[] commandNames = Command_Name.values();

        public static Command_Name getCommandName(int i) {
            return commandNames[i];
        }
    }

    public static enum Command_Example{
        _0_PVR_1_1COMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_y,
        _1_PVR_1_1COMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_y,
        _2,
        _3_PVR_1_1_A_60_30_10COMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_yCOMMA_A_DASH_ScreenCOMMA_60_DASH_Total_Silevr_SeatsCOMMA_30_DASH_GoldCOMMA_10_DASH_Platinum,
        _4_PVR_1_1_ACOMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_yCOMMA_A_DASH_Screen,
        _5_PVR_1_1_A_20220321_1200_1500_200_250_300_thor_engCOMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_yCOMMA_A_DASH_ScreenCOMMA_1200_DASH_From_TimeCOMMA_1500_DASH_To_TIMECOMMA_200_DASH_Silver_PriceCOMMA_250_DASH_GoldCOMMA300_DASH_Platinum,
        _6_PVR_1_1_A_20220321_1200_1500COMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_yCOMMA_A_DASH_ScreenCOMMA_1200_DASH_From_TimeCOMMA_1500_DASH_To_TIME,
        _7_PVR_1_1_A_20220321_1200_9_7_1COMMA__PVR_DASH_Multiplex_NameCOMMA_1_DASH_XCOMMA_1_DASH_yCOMMA_A_DASH_ScreenCOMMA_1200_DASH_From_TimeCOMMA_9_DASH_Silver_SeatsCOMMA_7_DASH_Gold_SeatsCOMMA_1_DASH_Platinum_Seats,
        _8_2_3_joker_engCOMMA__OB1_DASH_Sort_By_PriceCOMMA_2_DASH_Sort_By_ProximityCBCOMMA_OB1_DASH_Filter_By_Movie_NameCOMMA_2_DASH_Filter_By_Movie_LanguageCOMMA_3_DASH_Filter_By_Movie_Name_and_Movie_LanguageCBCOMMA_joker_DASH_Movie_NameCOMMA_eng_DASH_Movie_Language,
        _9;
        private static Command_Example[] commandExamples = Command_Example.values();

        public static Command_Example getCommandExample(int i) {
            return commandExamples[i];
        }
    }

    public Command(String command){
        List<String> commandSplit = new ArrayList<>(Arrays.asList(command.trim().split(" ")));
        this.command = Command_Name.getCommandName(Integer.valueOf(commandSplit.get(0)));
        commandSplit.remove(0);
        this.params = commandSplit;
    }

    public Command_Name getCommand() {
        return command;
    }

    public List<String> getParams() {
        return params;
    }
}
