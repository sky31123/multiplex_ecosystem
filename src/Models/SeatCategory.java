package Models;

public enum SeatCategory {
    SILVER, GOLD, PLATINUM;

    private static SeatCategory[] seatCatList = SeatCategory.values();

    public static SeatCategory getSeatCategory(int i) {
        return seatCatList[i];
    }
}
