package Models;

public class Occupation{
    int seats;
    float price;
    Occupation(int seats, float price){
        this.seats=seats;
        this.price=price;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public float getPrice() {
        return price;
    }

}