package Models;

import java.util.Objects;

public final class Show {

    final Seats occupied;
    final Movie movie;

    public Seats getOccupied() {
        return occupied;
    }

    public Movie getMovie() {
        return movie;
    }

    public Show(Seats occupied, Movie movie){
        this.occupied=occupied;
        this.movie=movie;
    }
}

