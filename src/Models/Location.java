package Models;

import java.awt.*;

public final class Location implements Comparable<Location>{

    final Point point;
    public Location(int x, int y){
        point = new Point(x,y);
    }

    public Point getPoint() {
        return point;
    }

    @Override
    public boolean equals(Object o) {
        Location anotherLocation = (Location) o;
        return this.point.equals(anotherLocation.point);
    }

    @Override
    public int hashCode() {
        return this.point.hashCode();
    }

    @Override
    public int compareTo(Location location) {
        Point currPoint = Customer.currPoint;
        if(this.point.distance(currPoint)<location.point.distance(currPoint))
            return -1;
        else if(this.point.distance(currPoint)>location.point.distance(currPoint))
            return 1;
        else
            return 0;
    }

    @Override
    public String toString() {
        return "("+this.point.x+","+this.point.y+")";
    }
}
