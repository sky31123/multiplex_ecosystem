package Models;

import java.util.HashMap;
import java.util.Map;

public final class Seats {
    public final Map<SeatCategory, Occupation> seat;
    public Seats(int silverSeatsOccupation, int goldSeatsOccupation, int platinumSeatsOccupation){
        seat = new HashMap<>();
        seat.put(SeatCategory.PLATINUM, new Occupation(platinumSeatsOccupation,0));
        seat.put(SeatCategory.GOLD, new Occupation(goldSeatsOccupation,0));
        seat.put(SeatCategory.SILVER, new Occupation(silverSeatsOccupation,0));
    }
    public Seats(float silverSeatPrice, float goldSeatPrice, float platinumSeatPrice){
        seat = new HashMap<>();
        seat.put(SeatCategory.PLATINUM, new Occupation(0,silverSeatPrice));
        seat.put(SeatCategory.GOLD, new Occupation(0,goldSeatPrice));
        seat.put(SeatCategory.SILVER, new Occupation(0,platinumSeatPrice));
    }

    public Map<SeatCategory, Occupation> getSeat() {
        return seat;
    }

    @Override
    public String toString() {
        return "[Silver-"+seat.get(SeatCategory.SILVER).seats+", Gold-"+seat.get(SeatCategory.GOLD).seats+", Platinum-"+seat.get(SeatCategory.PLATINUM).seats+"]";
    }
}
