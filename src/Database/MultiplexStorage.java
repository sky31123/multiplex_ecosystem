package Database;

import Filters.Filter;
import Models.*;

import java.util.*;

public interface MultiplexStorage {

    public void addMultiPlex(Multiplex multiplex);

    public void removeMultiplex(Multiplex multiplex);

    public void addScreen(Multiplex multiplex, Screen screen);

    public void removeScreen(Multiplex multiplex, Screen screen);

    public void addShow(Multiplex multiplex, Screen screen, TimeInterval timeInterval, Show show);

    public void removeShow(Multiplex multiplex, Screen screen, TimeInterval timeInterval);

    public void bookSeats(Multiplex multiplex, Screen screen, TimeInterval timeInterval, Seats seatsToBook);

    public List<Multiplex> getMultiplexes();

    public List<SearchResult> getShows(List<Filter> filters);
}