package Database;

import Exceptions.*;
import Filters.Filter;
import Models.*;

import java.util.*;

public class HashMapBasedMultiplexStorage implements MultiplexStorage{

    private static Map<Integer, Multiplex> storage = null;
    private static MultiplexStorage multiplexStorage = null;

    private HashMapBasedMultiplexStorage(){
        if(storage==null) {
            storage = new HashMap<>();
        }
    }

    public static synchronized MultiplexStorage getStorage(){
        if(multiplexStorage == null){
            multiplexStorage = new HashMapBasedMultiplexStorage();
        }
        return multiplexStorage;
    }


    @Override
    public void addMultiPlex(Multiplex multiplex) {
        if(storage.containsKey(multiplex.hashCode())){
            throw new MultiplexAlreadyExistsException();
        }
        else{
            storage.put(multiplex.hashCode(), multiplex);
        }
    }

    @Override
    public void removeMultiplex(Multiplex multiplex) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        else{
            storage.remove(multiplex.hashCode());
        }
    }

    @Override
    public void addScreen(Multiplex multiplex, Screen screen) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        if(storage.get(multiplex.hashCode()).getScreens().containsKey(screen.hashCode())){
            throw new ScreenExistsException();
        }
        storage.get(multiplex.hashCode()).getScreens().put(screen.hashCode(), screen);
    }

    @Override
    public void removeScreen(Multiplex multiplex, Screen screen) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().containsKey(screen.hashCode())){
            throw new ScreenNotFoundException();
        }
        storage.get(multiplex.hashCode()).getScreens().remove(screen.hashCode());
    }

    @Override
    public void addShow(Multiplex multiplex, Screen screen, TimeInterval timeInterval, Show show) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().containsKey(screen.hashCode())){
            throw new ScreenNotFoundException();
        }
        if(storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().containsKey(timeInterval))
            throw new ShowTimeOverlapsException();
        storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().put(timeInterval, show);
    }

    @Override
    public void removeShow(Multiplex multiplex, Screen screen, TimeInterval timeInterval) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().containsKey(screen.hashCode())){
            throw new ScreenNotFoundException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().containsKey(timeInterval))
            throw new ShowTimeNotExistsException();
        storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().remove(timeInterval);
    }

    @Override
    public void bookSeats(Multiplex multiplex, Screen screen, TimeInterval timeInterval, Seats seatsToBook) {
        if(!storage.containsKey(multiplex.hashCode())){
            throw new MultiplexNotExistsException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().containsKey(screen.hashCode())){
            throw new ScreenNotFoundException();
        }
        if(!storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().keySet().contains(timeInterval)){
            throw new TimeIntervalNotFoundException();
        }
        int matchingTimeIntervals = 0;
        for (TimeInterval existingTimeInterval : storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().keySet()){
            if(timeInterval.equals(existingTimeInterval))
                matchingTimeIntervals++;
        }
        if(matchingTimeIntervals>1)
            throw new MultipleCoincidingTimeIntervalsException();
        if(matchingTimeIntervals==0)
            throw new NoShowFoundException();
        Show show = storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getShowTimes().get(timeInterval);
        for(SeatCategory category:show.getOccupied().getSeat().keySet()){
            if(seatsToBook.getSeat().get(category).getSeats() > (storage.get(multiplex.hashCode()).getScreens().get(screen.hashCode()).getCapacity().getSeat().get(category).getSeats()-show.getOccupied().getSeat().get(category).getSeats()))
                throw new SeatsNotAvailableException();
        }
        for(SeatCategory category:show.getOccupied().getSeat().keySet()){
            show.getOccupied().getSeat().get(category).setSeats(show.getOccupied().getSeat().get(category).getSeats()+seatsToBook.getSeat().get(category).getSeats());
        }
    }

    @Override
    public List<Multiplex> getMultiplexes() {
        if(storage.size()==0){
            throw new NoMultiplexFoundException();
        }
        return new ArrayList<>(storage.values());
    }

    @Override
    public List<SearchResult> getShows(List<Filter> filters) {
        List<SearchResult> shows = new ArrayList<>();
        for (Multiplex mul:storage.values()) {
            for(Screen scr:mul.getScreens().values()){
                for(TimeInterval timeInterval:scr.getShowTimes().keySet()){
                    for (SeatCategory category:SeatCategory.values()) {
                        shows.add(new SearchResult(
                                scr.getShowTimes().get(timeInterval).getMovie().getName(),
                                scr.getShowTimes().get(timeInterval).getMovie().getLanguage(),
                                mul.getMultiplexName(),
                                mul.getMultiplexLocation().getPoint().x,
                                mul.getMultiplexLocation().getPoint().y,
                                scr.getScreenId(),
                                category.toString(),
                                scr.getCapacity().getSeat().get(category).getSeats() - scr.getShowTimes().get(timeInterval).getOccupied().getSeat().get(category).getSeats(),
                                timeInterval.getFromTime(),
                                scr.getShowTimes().get(timeInterval).getOccupied().getSeat().get(category).getPrice(),
                                mul.getMultiplexLocation()
                            ));
                    }
                }
            }
        }
        for (Filter filter:filters) {
            filter.filter(shows);
        }
        return shows;
    }


}
