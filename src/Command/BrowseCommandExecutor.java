package Command;

import Exceptions.InvalidCommandException;
import Filters.Filter;
import Filters.MovieLanguageFilter;
import Filters.MovieNameFilter;
import Models.Command;
import Models.SearchResult;
import Services.AdminService;
import Services.CustomerService;
import Sorters.LocationBasedSorter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BrowseCommandExecutor extends CommandExecutor{

    public static Command.Command_Name COMMAND = Command.Command_Name.BROWSE;

    public BrowseCommandExecutor(AdminService adminService, CustomerService customerService){
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 1 && command.getParams().size() != 3 && command.getParams().size() != 4){
            throw new InvalidCommandException();
        }
    }

    @Override
    public void execute(Command command) {
        List<SearchResult> searchResults = null;
        List<Filter> filters = new ArrayList<>();
        if(command.getParams().size()==4){
            filters.add(new MovieNameFilter(command.getParams().get(2)));
            filters.add(new MovieLanguageFilter(command.getParams().get(3)));
        }
        else if (command.getParams().size()==3){
            if(command.getParams().get(1).equals("1"))
                filters.add(new MovieNameFilter(command.getParams().get(2)));
            else
                filters.add(new MovieLanguageFilter(command.getParams().get(2)));
        }
        searchResults = customerService.getShows(filters);
        if(command.getParams().get(0).equals("1"))
            Collections.sort(searchResults);
        else
            Collections.sort(searchResults, new LocationBasedSorter());
        System.out.printf(SearchResult.OUTUPT_FORMAT+"\n","Movie","Language","Multiplex","Location","Screen","Category","Seats","Date","Time","Price","Distance");
        searchResults.forEach(System.out::println);
    }
}
