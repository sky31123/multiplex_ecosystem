package Command;

import Models.Command;
import Services.AdminService;
import Services.CustomerService;

abstract public class CommandExecutor {
    AdminService adminService;
    CustomerService customerService;
    CommandExecutor(AdminService adminService, CustomerService customerService){
        this.adminService=adminService;
        this.customerService=customerService;
    }

    public abstract void validate(Command command);
    public abstract void execute(Command command);
}
