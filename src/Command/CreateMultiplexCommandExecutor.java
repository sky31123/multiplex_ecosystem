package Command;

import Exceptions.InvalidCommandException;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.security.InvalidParameterException;

public class CreateMultiplexCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.CREATE_MULTIPLEX;

    public CreateMultiplexCommandExecutor(AdminService adminService, CustomerService customerService) {
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 3)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        super.adminService.addMultiplex(command.getParams().get(0), Integer.valueOf(command.getParams().get(1)), Integer.valueOf(command.getParams().get(2)));
        System.out.println("Multiplex "+ command.getParams().get(0) + " added at (" + command.getParams().get(1)+","+command.getParams().get(2)+")");
    }
}
