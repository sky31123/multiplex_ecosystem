package Command;

import Exceptions.Multiplex_Ecosystem_Exception;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class CommandExecutorFactory {
    private Map<Command.Command_Name, CommandExecutor> commandMap;
    public CommandExecutorFactory(AdminService adminService, CustomerService customerService){
        this.commandMap = new HashMap<>();
        commandMap.put(CreateMultiplexCommandExecutor.COMMAND, new CreateMultiplexCommandExecutor(adminService, customerService));
        commandMap.put(RemoveMultiplexCommandExecutor.COMMAND, new RemoveMultiplexCommandExecutor(adminService, customerService));
        commandMap.put(ShowMultiplexCommandExecutor.COMMAND, new ShowMultiplexCommandExecutor(adminService, customerService));
        commandMap.put(AddScreenCommandExecutor.COMMAND, new AddScreenCommandExecutor(adminService, customerService));
        commandMap.put(RemoveScreenCommandExecutor.COMMAND, new RemoveScreenCommandExecutor(adminService, customerService));
        commandMap.put(AddShowCommandExecutor.COMMAND, new AddShowCommandExecutor(adminService, customerService));
        commandMap.put(RemoveShowCommandExecutor.COMMAND, new RemoveShowCommandExecutor(adminService, customerService));
        commandMap.put(BrowseCommandExecutor.COMMAND, new BrowseCommandExecutor(adminService, customerService));
        commandMap.put(BookShowCommandExecutor.COMMAND, new BookShowCommandExecutor(adminService, customerService));
    }
    public void process() throws IOException {
        File file = new File("src/populate_multiplex_ecosystem.txt");
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String input = reader.readLine();
        while(input != null){
            Command command = new Command(input);
            if (command.getCommand() == Command.Command_Name.EXIT) {
                break;
            }
            commandMap.get(command.getCommand()).validate(command);
            commandMap.get(command.getCommand()).execute(command);
            input = reader.readLine();
        }
        reader = new BufferedReader(new InputStreamReader(System.in));
        while(true){
            try {
                System.out.println("---------------------------------------\nChoose one of the option!");
                for (short i = 0; i < Command.Command_Name.values().length; i++) {
                    System.out.println(i + " - " + Command.Command_Name.getCommandName(i)+" - ex. "+
                            Command.Command_Example.getCommandExample(i).toString()
                                    .replaceAll("_"," ")
                                    .replaceAll("DASH","->")
                                    .replaceAll("COMMA",",")
                    .replaceAll("OB","(")
                    .replaceAll("CB",")"));
                }
                System.out.println("---------------------------------------");
                Command command = new Command(reader.readLine());
                if (command.getCommand() == Command.Command_Name.EXIT) {
                    break;
                }
                commandMap.get(command.getCommand()).validate(command);
                commandMap.get(command.getCommand()).execute(command);
            }
            catch (Multiplex_Ecosystem_Exception e){
                //do nothing
            }
            catch (RuntimeException e){
                throw new RuntimeException(e);
            }
        }
        System.out.println("Existing Multiplex_Ecosystem..");
    }
}