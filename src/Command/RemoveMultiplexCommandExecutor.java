package Command;

import Database.MultiplexStorage;
import Exceptions.InvalidCommandException;
import Models.Command;
import Models.Multiplex;
import Services.AdminService;
import Services.CustomerService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Set;

public class RemoveMultiplexCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.REMOVE_MULTIPLEX;

    RemoveMultiplexCommandExecutor(AdminService adminService, CustomerService customerService) {
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 3)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        super.adminService.removeMultiplex(command.getParams().get(0), Integer.valueOf(command.getParams().get(1)), Integer.valueOf(command.getParams().get(2)));
        System.out.println("Multiplex "+ command.getParams().get(0) + " removed at (" + command.getParams().get(1)+","+command.getParams().get(2)+")");
    }
}
