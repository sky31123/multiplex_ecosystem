package Command;

import Exceptions.InvalidCommandException;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class AddShowCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.ADD_SHOW;
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmm");
    public AddShowCommandExecutor(AdminService adminService, CustomerService customerService){
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 12)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        try {
            adminService.addShow(
                    command.getParams().get(0),
                    Integer.valueOf(command.getParams().get(1)),
                    Integer.valueOf(command.getParams().get(2)),
                    command.getParams().get(3).charAt(0),
                    formatter.parse(command.getParams().get(4)+" "+command.getParams().get(5)).getTime(),
                    formatter.parse(command.getParams().get(4)+" "+command.getParams().get(6)).getTime(),
                    Float.valueOf(command.getParams().get(7)),
                    Float.valueOf(command.getParams().get(8)),
                    Float.valueOf(command.getParams().get(9)),
                    command.getParams().get(10),
                    command.getParams().get(11));
            System.out.printf("%s[%s] show added at screen-%s for %s %s-%s in %s at (%s,%s).\n",
                                                            command.getParams().get(10),
                                                            command.getParams().get(11),
                                                            command.getParams().get(3),
                                                            command.getParams().get(4),
                                                            command.getParams().get(5),
                                                            command.getParams().get(6),
                                                            command.getParams().get(0),
                                                            command.getParams().get(1),
                                                            command.getParams().get(2));
        System.out.println("show added!");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
