package Command;

import Exceptions.InvalidCommandException;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.awt.print.Book;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class BookShowCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.BOOK_SHOW;
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd HHmm");

    public BookShowCommandExecutor(AdminService adminService, CustomerService customerService){
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size()!=7 && command.getParams().size()!=8 && command.getParams().size()!=9)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        int silverSeats = 0;
        int goldSeats = 0;
        int platinumSeats = 0;
        if(command.getParams().size()==7)
            silverSeats = Integer.valueOf(command.getParams().get(6));
        else if(command.getParams().size()==8) {
            silverSeats = Integer.valueOf(command.getParams().get(6));
            goldSeats = Integer.valueOf(command.getParams().get(7));
        }
        else{
            silverSeats = Integer.valueOf(command.getParams().get(6));
            goldSeats = Integer.valueOf(command.getParams().get(7));
            platinumSeats = Integer.valueOf(command.getParams().get(8));
        }
        try {
            customerService.bookSeats(
                    command.getParams().get(0),
                    Integer.valueOf(command.getParams().get(1)),
                    Integer.valueOf(command.getParams().get(2)),
                    command.getParams().get(3).charAt(0),
                    formatter.parse(command.getParams().get(4)+" "+command.getParams().get(5)).getTime(),
                    silverSeats,
                    goldSeats,
                    platinumSeats
                    );
            System.out.printf("[Silver-%s, Gold-%s, Platinum-%s] tickets booked for the show %s %s in %s at (%s,%s).\n",
                                                        command.getParams().get(6),
                                                        command.getParams().get(7),
                                                        command.getParams().get(8),
                                                        command.getParams().get(4),
                                                        command.getParams().get(5),
                                                        command.getParams().get(0),
                                                        command.getParams().get(1),
                                                        command.getParams().get(2));
            System.out.println("Tickets Booked!");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
