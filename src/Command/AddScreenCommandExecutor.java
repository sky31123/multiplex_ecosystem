package Command;

import Exceptions.InvalidCommandException;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.security.InvalidParameterException;

public class AddScreenCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.ADD_SCREEN;

    AddScreenCommandExecutor(AdminService adminService, CustomerService customerService){
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 7)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        super.adminService.addScreen( command.getParams().get(0),
                                        Integer.valueOf(command.getParams().get(1)),
                                        Integer.valueOf(command.getParams().get(2)),
                                        command.getParams().get(3).charAt(0),
                                        Short.valueOf(command.getParams().get(4)),
                                        Short.valueOf(command.getParams().get(5)),
                                        Short.valueOf(command.getParams().get(6)));
        System.out.println("Screen-"+ command.getParams().get(3).charAt(0) +
                            " added in " + command.getParams().get(0) +"["+Integer.valueOf(command.getParams().get(1))+","+Integer.valueOf(command.getParams().get(2))+"]"+
                            " with [Silver-"+Short.valueOf(command.getParams().get(4))+", Gold-"+Short.valueOf(command.getParams().get(5))+", Platinum-"+Short.valueOf(command.getParams().get(6))+
                            "] capacity.");
    }
}
