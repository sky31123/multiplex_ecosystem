package Command;

import Exceptions.InvalidCommandException;
import Models.Command;
import Services.AdminService;
import Services.CustomerService;

import java.security.InvalidParameterException;

public class RemoveScreenCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.REMOVE_SCREEN;

    RemoveScreenCommandExecutor(AdminService adminService, CustomerService customerService){
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        if(command.getParams().size() != 4)
            throw new InvalidCommandException();
    }

    @Override
    public void execute(Command command) {
        super.adminService.removeScreen( command.getParams().get(0),
                Integer.valueOf(command.getParams().get(1)),
                Integer.valueOf(command.getParams().get(2)),
                command.getParams().get(3).charAt(0));
        System.out.println("Screen-"+ command.getParams().get(3).charAt(0) +
                " removed from " + command.getParams().get(0) +"["+Integer.valueOf(command.getParams().get(1))+","+Integer.valueOf(command.getParams().get(2))+"]");
    }
}
