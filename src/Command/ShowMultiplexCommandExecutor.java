package Command;

import Database.MultiplexStorage;
import Exceptions.InvalidCommandException;
import Models.Command;
import Models.Multiplex;
import Services.AdminService;
import Services.CustomerService;

import java.util.*;

public class ShowMultiplexCommandExecutor extends CommandExecutor {

    public static Command.Command_Name COMMAND = Command.Command_Name.SHOW_MULTIPLEX;

    ShowMultiplexCommandExecutor(AdminService adminService, CustomerService customerService) {
        super(adminService, customerService);
    }

    @Override
    public void validate(Command command) {
        return;
    }

    @Override
    public void execute(Command command) {
        System.out.println("Here's all Multiplexes!");
        List<Multiplex> list = super.adminService.getMultiplexes();
        Collections.sort(list);
        list.forEach(System.out::println);
    }
}
