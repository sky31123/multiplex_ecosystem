package Services;

import Database.HashMapBasedMultiplexStorage;
import Database.MultiplexStorage;
import Filters.Filter;
import Models.*;

import java.util.List;

public class CustomerService {

    private static CustomerService service = null;
    private static MultiplexStorage storage = null;

    private CustomerService(){
        storage = HashMapBasedMultiplexStorage.getStorage();
    }

    public static synchronized CustomerService getCustomerService(){
        if(service == null) {
            service = new CustomerService();
        }
        return service;
    }

    public List<SearchResult> getShows(List<Filter> filters){
        return storage.getShows(filters);
    }

    public void bookSeats(String multiplexName,
                          int x,
                          int y,
                          char screenId,
                          long fromTime,
                          int silverSeats,
                          int goldSeats,
                          int platinumSeats){
        Multiplex tempMultiplex = new Multiplex(multiplexName, new Location(x,y));
        Screen tempScreen = new Screen(screenId);
        TimeInterval timeInterval = new TimeInterval(fromTime, -1);
        Seats tempSeats = new Seats(silverSeats, goldSeats, platinumSeats);
        storage.bookSeats(tempMultiplex, tempScreen, timeInterval, tempSeats);
    }
}
