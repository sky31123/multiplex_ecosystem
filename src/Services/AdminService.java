package Services;

import Database.HashMapBasedMultiplexStorage;
import Database.MultiplexStorage;
import Models.*;

import java.util.List;

public class AdminService {

    private static AdminService service = null;
    private static MultiplexStorage storage = null;

    private AdminService(){
        storage = HashMapBasedMultiplexStorage.getStorage();
    }

    public static synchronized AdminService getAdminService(){
        if(service == null) {
            service = new AdminService();
        }
            return service;
    }

    public void addMultiplex(String multiplexName, int x, int y){
        storage.addMultiPlex(new Multiplex(multiplexName, new Location(x,y)));
    }

    public void removeMultiplex(String multiplexName, int x, int y){
        storage.removeMultiplex(new Multiplex(multiplexName, new Location(x,y)));
    }

    public List<Multiplex> getMultiplexes(){
        return storage.getMultiplexes();
    }

    public void addScreen(String multiplexName, int x, int y, char screenId, short silverSeats, short goldSeats, short platinumSeats){
        Seats seats = new Seats(silverSeats, goldSeats, platinumSeats);
        Screen screen = new Screen(screenId, seats);
        Multiplex tempMultiplex = new Multiplex(multiplexName, new Location(x,y));
        storage.addScreen(tempMultiplex, screen);
    }

    public void removeScreen(String multiplexName, int x, int y, char screenId){
        Screen tempScreen = new Screen(screenId);
        Multiplex tempMultiplex = new Multiplex(multiplexName, new Location(x,y));
        storage.removeScreen(tempMultiplex, tempScreen);
    }

    public void addShow(String multimplexName,
                        int x,
                        int y,
                        char screenId,
                        long fromTime,
                        long toTime,
                        float silverSeatPrice,
                        float goldSeatPrice,
                        float platinumSeatPrice,
                        String movieName,
                        String movieLang){
        Movie movie = new Movie(movieName, movieLang);
        TimeInterval timeInterval = new TimeInterval(fromTime, toTime);
        Seats seats = new Seats(silverSeatPrice, goldSeatPrice, platinumSeatPrice);
        Show show = new Show(seats, movie);
        Multiplex tempMultiplex = new Multiplex(multimplexName, new Location(x,y));
        Screen tempScreen = new Screen(screenId);
        storage.addShow(tempMultiplex, tempScreen, timeInterval, show);
    }

    public void removeShow(String multimplexName,
                           int x,
                           int y,
                           char screenId,
                           long fromTime,
                           long toTime){
        TimeInterval timeInterval = new TimeInterval(fromTime, toTime);
        Multiplex tempMultiplex = new Multiplex(multimplexName, new Location(x,y));
        Screen tempScreen = new Screen(screenId);
        storage.removeShow(tempMultiplex, tempScreen, timeInterval);
    }
}
