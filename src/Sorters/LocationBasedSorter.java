package Sorters;

import Models.SearchResult;

import java.util.Comparator;

public class LocationBasedSorter implements Comparator<SearchResult> {
    @Override
    public int compare(SearchResult s1, SearchResult s2) {
        if(s1.distance < s2.distance)
            return -1;
        else if (s1.distance > s2.distance)
            return 1;
        else
            return 0;
    }
}
