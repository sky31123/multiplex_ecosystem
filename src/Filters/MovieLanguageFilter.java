package Filters;

import Models.SearchResult;

import java.util.List;

public class MovieLanguageFilter implements Filter {

    String movieLanguage;

    public MovieLanguageFilter(String movieLanguage){
        this.movieLanguage = movieLanguage;
    }

    @Override
    public void filter(List<SearchResult> searchResults) {
        searchResults.removeIf(s->!s.movieLanguage.equals(this.movieLanguage));
    }
}