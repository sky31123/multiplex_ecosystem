package Filters;

import Models.SearchResult;

import java.util.List;

public class MovieNameFilter implements Filter {

    String movieName;

    public MovieNameFilter(String movieName){
        this.movieName = movieName;
    }

    @Override
    public void filter(List<SearchResult> searchResults) {
        searchResults.removeIf(s->!s.movie.equals(this.movieName));
    }
}