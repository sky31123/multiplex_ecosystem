package Filters;

import Models.SearchResult;

import java.util.List;

public interface Filter {
    public void filter(List<SearchResult> searchResults);
}