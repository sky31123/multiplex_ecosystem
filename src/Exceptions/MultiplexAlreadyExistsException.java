package Exceptions;

public class MultiplexAlreadyExistsException extends Multiplex_Ecosystem_Exception {
    public MultiplexAlreadyExistsException(){
        System.out.println("Multiple already exists! Please try again!");
    }
}
