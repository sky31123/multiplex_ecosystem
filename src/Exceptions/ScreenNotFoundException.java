package Exceptions;

public class ScreenNotFoundException extends Multiplex_Ecosystem_Exception {
    public ScreenNotFoundException(){
        System.out.println("Screen doesn't exist with given screen id in this multiplex!");
    }
}
