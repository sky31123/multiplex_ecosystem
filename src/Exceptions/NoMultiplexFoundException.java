package Exceptions;

public class NoMultiplexFoundException extends Multiplex_Ecosystem_Exception {
    public NoMultiplexFoundException(){
        System.out.println("No Multiplex Found!");
    }
}
