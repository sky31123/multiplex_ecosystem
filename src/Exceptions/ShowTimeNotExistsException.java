package Exceptions;

public class ShowTimeNotExistsException extends Multiplex_Ecosystem_Exception {
    public ShowTimeNotExistsException(){
        System.out.println("Show Time doesn't exists!");
    }
}
