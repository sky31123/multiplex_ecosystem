package Exceptions;

public class ShowTimeOverlapsException extends Multiplex_Ecosystem_Exception {
    public ShowTimeOverlapsException(){
        System.out.println("Show Time overlaps with existing show!");
    }
}
