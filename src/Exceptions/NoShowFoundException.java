package Exceptions;

public class NoShowFoundException extends Multiplex_Ecosystem_Exception{
    public NoShowFoundException(){
        System.out.println("No Show found for given time!");
    }
}
