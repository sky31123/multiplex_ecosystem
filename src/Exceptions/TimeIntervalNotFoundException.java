package Exceptions;

public class TimeIntervalNotFoundException extends Multiplex_Ecosystem_Exception {
    public TimeIntervalNotFoundException(){
        System.out.println("No show for given time!");
    }
}
