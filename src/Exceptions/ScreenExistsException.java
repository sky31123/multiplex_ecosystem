package Exceptions;

public class ScreenExistsException extends Multiplex_Ecosystem_Exception {
    public ScreenExistsException(){
        System.out.println("Screen exists with given screen id in this multiplex!");
    }
}
