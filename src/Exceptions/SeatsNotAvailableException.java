package Exceptions;

public class SeatsNotAvailableException extends Multiplex_Ecosystem_Exception {
    public SeatsNotAvailableException(){
        System.out.println("Not Enough Seats!");
    }
}
