import Command.CommandExecutorFactory;
import Command.CreateMultiplexCommandExecutor;
import Models.Command;
import Models.Location;
import Models.SearchResult;
import Services.AdminService;
import Services.CustomerService;
import org.xml.sax.helpers.LocatorImpl;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Driver {
    public static void main(String[] args) throws IOException, ParseException {
        System.out.println("Welcome to Multiplex Ecosystem!");
        AdminService adminService = AdminService.getAdminService();
        CustomerService customerService = CustomerService.getCustomerService();
        CommandExecutorFactory factory = new CommandExecutorFactory(adminService, customerService);
        factory.process();
        /*ArrayList<Integer> list  = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        m(list);
        System.out.println(list);*/
    }

    public static void m(ArrayList<Integer> list){
        list.remove(0);
    }
}
